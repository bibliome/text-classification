#!/usr/bin/env python

"""The setup script."""

from setuptools import setup, find_packages

with open('README.rst') as readme_file:
    readme = readme_file.read()

with open('HISTORY.rst') as history_file:
    history = history_file.read()

requirements = [
    'accelerate==0.18.0',
    'black==21.7b0',
    'bump2version==0.5.11',
    'confection==0.1.0',
    'coverage==4.5.4',
    'easydict==1.10',
    'flake8==3.7.8',
    'fqdn==1.5.1',
    'isoduration==20.11.0',
    'jsonpickle==3.0.1',
    'jsonpointer==2.3',
    'jupyter==1.0.0',
    'langcodes==3.3.0',
    'lxml==4.9.2',
    'noisereduce==2.0.1',
    'optuna==3.1.1',
    'protobuf==3.20.0',
    'pybind11==2.10.4',
    'pytest==6.2.4',
    'sphinx==1.8.5',
    "tox==3.14.0",
    'twine==1.14.0',
    'uri-template==1.2.0',
    'watchdog==0.9.0',
    "webcolors==1.13",
]

test_requirements = ['pytest>=3', ]

setup(
    author="Luis Antonio VASQUEZ",
    author_email='luis-antonio.vasquez-reina@inrae.fr',
    python_requires='>=3.6',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Natural Language :: English',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
    ],
    description="Text classification for the projects of the Bibliome group at MaIAGE, INRAE",
    entry_points={
        'console_scripts': [
            'text_classification_bibliome=text_classification_bibliome.cli:main',
        ],
    },
    install_requires=requirements,
    license="MIT license",
    long_description=readme + '\n\n' + history,
    include_package_data=True,
    keywords='text_classification_bibliome',
    name='text_classification_bibliome',
    packages=find_packages(
        include=['text_classification_bibliome', 'text_classification_bibliome.*']),
    test_suite='tests',
    tests_require=test_requirements,
    url='https://github.com/luis-antonio.vasquez-reina/text_classification_bibliome',
    version='0.1.0',
    zip_safe=False,
)
